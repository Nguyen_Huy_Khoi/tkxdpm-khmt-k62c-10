package com.ecobike.db;

import java.util.ArrayList;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;

public interface IDatabase {
	public ArrayList<Bike> getAllBike(Class<?> bikeClass);
	public <E> E createBike(E bike);
	public ArrayList<Station> getAllStation();
}
