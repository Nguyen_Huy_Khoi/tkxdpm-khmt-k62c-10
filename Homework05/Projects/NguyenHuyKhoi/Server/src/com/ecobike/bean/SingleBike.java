package com.ecobike.bean;


import java.util.ArrayList;
import java.util.Date;

public class SingleBike extends Bike{

	public SingleBike() {
		super();
	}
	
	public SingleBike(String id, String name, String type, float weight, String licensePlate,	
			String manufacturingDate, String producer, float cost,String stationId) {
		super(id,name,type,weight,licensePlate,manufacturingDate,producer,cost,stationId);
	}
	
	
}