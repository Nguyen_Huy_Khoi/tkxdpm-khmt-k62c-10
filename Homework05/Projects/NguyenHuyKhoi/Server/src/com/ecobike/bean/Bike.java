package com.ecobike.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("bike")
@JsonSubTypes({ 
	@Type(value = SingleBike.class, name = "singlebike"),
	@Type(value = TwinBike.class, name = "twinbike") ,
	@Type(value = EBike.class, name = "ebike")
})

public class Bike {
	
	private String id;
	private String name;
	private String type;
	private float weight;
	private String licensePlate;
	private String manufacturingDate;
	private String producer;
	private float cost;
	private String stationId;

	public Bike() {
		super();
	}
	
	public Bike(String id, String name, String type, float weight, String licensePlate,	
				String manufacturingDate, String producer, float cost,String stationId) {
		this.id=id;
		this.name = name;
		this.type = type;
		this.weight=weight;
		this.licensePlate=licensePlate;
		this.manufacturingDate=manufacturingDate;
		this.producer=producer;
		this.cost=cost;
		this.stationId=stationId;


	}
	
	

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(String manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}

	
	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	@Override
	public String toString() {
		return "Bike [id=" + id + ", name=" + name + ", type=" + type + ", weight=" + weight + ", licensePlate="
				+ licensePlate + ", manufacturingDate=" + manufacturingDate + ", producer=" + producer + ", cost="
				+ cost + "]";
	}


	


}