package com.ecobike.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ecobike.bean.Bike;
import com.ecobike.db.IDatabase;
import com.ecobike.db.JsonDatabase;

@Path("/bike")
public class BikeService {
	
	private IDatabase database;
	
	public BikeService() {
		database = JsonDatabase.singleton();
	}

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getAllBike() {
    	ArrayList<Bike> res = database.getAllBike(Bike.class);
        return res;
    }
    
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike createBike(Bike bike) {
		System.out.println("BikeService CreateBike :"+bike.toString());
		return database.createBike(bike);
	}
}