package com.ecobike.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ecobike.bean.Bike;
import com.ecobike.bean.Station;
import com.ecobike.db.IDatabase;
import com.ecobike.db.JsonDatabase;

@Path("/station")
public class StationService {
	
	private IDatabase database;
	
	public StationService() {
		database = JsonDatabase.singleton();
	}

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getAllStation() {
    	ArrayList<Station> res = database.getAllStation();
        return res;
    }
    
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike createBike(Bike bike) {
		System.out.println("BikeService CreateBike :"+bike.toString());
		return database.createBike(bike);
	}
}