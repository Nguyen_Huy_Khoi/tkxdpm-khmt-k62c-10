package com.ecobike.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.TwinBike;
import com.ecobike.db.IDatabase;
import com.ecobike.db.JsonDatabase;

@Path("/ebike")
public class EBikeService {

	private IDatabase database;

	public EBikeService() {
		database = JsonDatabase.singleton();
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getAllEBike() {
		ArrayList<Bike> res = database.getAllBike(EBike.class);
	    return res;
	}
	    
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public EBike createEBike(EBike bike) {
		System.out.println("EBikeService CreateBike :"+bike.toString());
		return database.createBike(bike);
	}
}