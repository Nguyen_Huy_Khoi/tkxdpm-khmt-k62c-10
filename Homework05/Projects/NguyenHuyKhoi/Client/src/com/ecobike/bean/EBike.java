package com.ecobike.bean;



import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;


public class EBike extends Bike {
	private int batteryPercentage;
	private int loadCycles;
	private int estimatedUsageTimeRemaining;
	
	public EBike() {
		super();
	}
	
	public EBike(String id, String name, String type, float weight, String licensePlate,	
			String manufacturingDate, String producer, float cost,String stationId) {
		super(id,name,type,weight,licensePlate,manufacturingDate,producer,cost,stationId);
	}
	
	public EBike(String id, String name, String type, float weight, String licensePlate,	
			String manufacturingDate, String producer, float cost,
			int batteryPercentage,int loadCycles,int estimatedUsageTimeRemaining,String stationId ) {
		
		super(id,name,type,weight,licensePlate,manufacturingDate,producer,cost,stationId);
		this.batteryPercentage=batteryPercentage;
		this.loadCycles=loadCycles;
		this.estimatedUsageTimeRemaining=estimatedUsageTimeRemaining;
	}
	

	

	public int getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public int getLoadCycles() {
		return loadCycles;
	}

	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}

	public int getEstimatedUsageTimeRemaining() {
		return estimatedUsageTimeRemaining;
	}

	public void setEstimatedUsageTimeRemaining(int estimatedUsageTimeRemaining) {
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	@Override
	public String toString() {
		return super.toString()+ "EBike [batteryPercentage=" + batteryPercentage + ", loadCycles=" + loadCycles
				+ ", estimatedUsageTimeRemaining=" + estimatedUsageTimeRemaining + "]";
	}
	
	
}