package com.ecobike.components.abstracts.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.ecobike.page.controller.AController;
import com.ecobike.page.controller.ADataTypeController;
import com.ecobike.page.controller.AdminBikeAddController;

@SuppressWarnings("serial")
public abstract class ADataAddPane<T> extends JPanel {
	protected T t;
	protected GridBagLayout layout;
	protected GridBagConstraints c;
	
	protected NumberFormat numberFormat;
	protected DateFormat dateFormat;
	
	protected ADataTypeController<T>  controller;
	

	public ADataAddPane(ADataTypeController<T> controller) {
		layout = new GridBagLayout();
		this.setLayout(layout);
		
		this.controller=controller;

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		numberFormat = NumberFormat.getNumberInstance();
		dateFormat = DateFormat.getDateInstance();
		buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 1;
		c.gridy = row;
		JButton addButton = new JButton("Add");
		add(addButton, c);
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Trigger Press Add Button");
				
				if (!validateFields()) {
					JOptionPane.showMessageDialog(null, "Hãy điền đủ và đúng định dạng tất cả các trường.");
					return ;
				};
				T newObjT  = getObject();
				//controller.create(getObject());
				if (controller.create(newObjT)!=null) {
					JOptionPane.showMessageDialog(null, "Đã thêm dữ liệu thành công!");
				};
				
			
				resetFields();
			}
		});
		

		// Empty label for resizing
//		c.weightx = 1;
//		c.gridx = 3;
//		c.gridy = row - 1;
//		add(new JLabel(), c);
	}
	
//	public ADataAddPane(IDataManageController<T> controller) {
//		this();
//		this.controller = controller;
//	}
	
	public abstract void buildControls();

	
	public abstract T getObject();
	
	public abstract void resetFields();
	
	public abstract boolean validateFields();
	
	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	
	

}
