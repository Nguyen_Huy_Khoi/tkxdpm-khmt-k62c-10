package com.ecobike.components.bike.singlebike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.SingleBike;
import com.ecobike.components.bike.gui.BikeAddPane;
import com.ecobike.page.controller.ADataTypeController;

@SuppressWarnings("serial")
public class SingleBikeAddPane extends BikeAddPane {
	
	public SingleBikeAddPane(ADataTypeController<Bike> controller) {
		super(controller);
		this.t=new SingleBike();
		getTypeField().setText("Xe đạp đơn");
		
	}
	
}
