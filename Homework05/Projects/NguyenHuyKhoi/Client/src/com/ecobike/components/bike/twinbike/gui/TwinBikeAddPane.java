package com.ecobike.components.bike.twinbike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.TwinBike;
import com.ecobike.components.bike.gui.BikeAddPane;
import com.ecobike.page.controller.ADataTypeController;

@SuppressWarnings("serial")
public class TwinBikeAddPane extends BikeAddPane {
	
	public TwinBikeAddPane(ADataTypeController<Bike> controller) {
		super(controller);
		this.t=new TwinBike();
		getTypeField().setText("Xe đạp đôi");
	}
	
}
