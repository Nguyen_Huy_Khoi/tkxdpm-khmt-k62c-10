package com.ecobike.components.bike.ebike.gui;

import java.util.Map;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.components.bike.gui.BikeAddPane;
import com.ecobike.page.controller.ADataTypeController;

@SuppressWarnings("serial")
public class EBikeAddPane extends BikeAddPane {
	
	private JFormattedTextField batteryPercentageField;
	private JFormattedTextField loadCyclesField;
	private JFormattedTextField estimatedUsageTimeRemainingField;

	public EBikeAddPane(ADataTypeController<Bike> controller) {
		super(controller);
		this.t=new EBike();
		getTypeField().setText("Xe đạp điện");
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
		
		JLabel batteryPercentageLabel = new JLabel("Dung lượng pin(%): ");
		batteryPercentageField = new JFormattedTextField(numberFormat);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(batteryPercentageLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(batteryPercentageField, c);
		
		JLabel loadCyclesLabel = new JLabel("Số lần sạc:");
		loadCyclesField = new JFormattedTextField(numberFormat);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(loadCyclesLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(loadCyclesField, c);
		
		JLabel estimatedUsageTimeRemainingLabel = new JLabel("Thời gian sử dụng còn lại:");
		estimatedUsageTimeRemainingField = new JFormattedTextField(numberFormat);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(estimatedUsageTimeRemainingLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(estimatedUsageTimeRemainingField, c);
	}
	
	
	@Override
	public Bike getObject() {
		super.getObject();
		if (t instanceof EBike) {
			System.out.println("EBike here ");
			EBike eBike=(EBike) t;
			eBike.setBatteryPercentage(Integer.valueOf(batteryPercentageField.getText().replace(",", "")));
			eBike.setLoadCycles(Integer.valueOf(loadCyclesField.getText().replace(",", "")));
			eBike.setEstimatedUsageTimeRemaining(Integer.valueOf(estimatedUsageTimeRemainingField.getText().replace(",", "")));
			return eBike;
		}
		
		System.out.println("not Ebike here ");
		return t;
	}
	
	@Override
	public void resetFields() {
		super.resetFields();
		this.batteryPercentageField.setText("");
		this.loadCyclesField.setText("");
		this.estimatedUsageTimeRemainingField.setText("");
	}
	
	@Override
	public boolean validateFields() {
		
		if (!super.validateFields()) return false;
		String batField=batteryPercentageField.getText().replace(",", "");
		if ("".equals(batField)) return false;
		if ("".equals(loadCyclesField.getText())) return false;
		if ("".equals(estimatedUsageTimeRemainingField.getText())) return false;
		
		
		float f=Float.valueOf(batField);
		if (f<0 || f>100) return false;
		
		
		return true;	
	}
}
