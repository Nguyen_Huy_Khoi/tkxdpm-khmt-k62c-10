package com.ecobike.page.controller;

import javax.swing.JPanel;

import com.ecobike.bean.Bike;

import com.ecobike.components.bike.ebike.gui.EBikeAddPane;
import com.ecobike.components.bike.twinbike.gui.TwinBikeAddPane;
import com.ecobike.factory.ControllerFactory;
import com.ecobike.factory.FactoryItemId;
import com.ecobike.page.gui.APage;
import com.ecobike.page.gui.AdminBikeManagePage;
public class AdminBikeManageController extends AController{

	static {
		ControllerFactory.getInstance().registerController(
				FactoryItemId.BIKE_MANAGE_PAGE
				, 
				new AdminBikeManageController()
		);
	}
	
	
	public AdminBikeManageController(APage page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public AdminBikeManageController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public AController createController() {
		// TODO Auto-generated method stub
		return new AdminBikeManageController();
	}
	

	

	

}
