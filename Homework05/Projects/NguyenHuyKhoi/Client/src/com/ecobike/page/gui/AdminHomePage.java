package com.ecobike.page.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ecobike.factory.FactoryItemId;
import com.ecobike.factory.PageFactory;
import com.ecobike.main.AdminMain;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JTextField;

public class AdminHomePage extends APage {

	static {
		PageFactory.getInstance().registerPage(
				FactoryItemId.ADMIN_HOME_PAGE
				, 
				new AdminHomePage()
		);
	}
	
	private JFrame parent;
	
	@Override
	public APage createPage(JFrame parent) {
		return new AdminHomePage(parent);
	}
	
	public AdminHomePage() {
		
	}
	
	public AdminHomePage(JFrame parent) {
		this.parent=parent;
		this.setLayout(null);
		
		JLabel header = new JLabel();
		header.setText("Chọn chức năng");
		header.setFont(new Font("Tahoma", Font.PLAIN, 25));
		header.setBounds(245, 15, 247, 30);
		this.add(header);
		
		JButton btnStationManage = new JButton("Quản lý bãi xe");
		btnStationManage.setBounds(120, 80, 160, 80);
		this.add(btnStationManage);
		
		JButton btnBikeManage = new JButton("Quản lý xe ");
		btnBikeManage.setBounds(120, 200, 160, 80);
		this.add(btnBikeManage);
		
		btnBikeManage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Chuyển sang màn quản lý xe");
				((AdminMain)parent).displayPage(FactoryItemId.BIKE_MANAGE_PAGE);
			}
		});
		
		JButton btnRentedBikeManage = new JButton("Quản lý xe đang thuê ");
		btnRentedBikeManage.setBounds(360, 80, 160, 80);
		this.add(btnRentedBikeManage);
		
		JButton btnBillManage = new JButton("Quản lý hóa đơn ");
		btnBillManage.setBounds(360, 200, 160, 80);
		this.add(btnBillManage);
		
	
		
	}



	public JFrame getParent() {
		return parent;
	}


	public void setParent(JFrame parent) {
		this.parent = parent;
	}




	
	
}
