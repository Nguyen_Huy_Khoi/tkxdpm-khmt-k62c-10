package com.ecobike.page.controller;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.ecobike.bean.Bike;
import com.ecobike.factory.ControllerFactory;
import com.ecobike.factory.FactoryItemId;
import com.ecobike.factory.PageFactory;
import com.ecobike.page.gui.APage;
import com.ecobike.page.gui.AdminHomePage;
public class AdminHomeController extends AController{
	static {
		ControllerFactory.getInstance().registerController(
				FactoryItemId.ADMIN_HOME_PAGE
				, 
				new AdminHomeController()
		);
	}
	

	
	public AdminHomeController(APage page) {
		super(page);
		// TODO Auto-generated constructor stub
	}

	public AdminHomeController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public AController createController() {
		return new AdminHomeController();
	}
	
}
