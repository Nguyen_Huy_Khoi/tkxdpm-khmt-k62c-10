package com.ecobike.test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ValidateLicensePlateBlackBoxTest.class, ValidateLicenseWhiteBoxTest.class})
public class ValidateLicensePlateText {

}