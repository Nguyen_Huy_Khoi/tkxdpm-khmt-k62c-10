package com.ecobike.test;
import org.junit.Test;
import org.junit.platform.engine.support.filter.ClasspathScanningSupport;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ecobike.components.bike.gui.BikeAddPane;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

@RunWith(Parameterized.class)
public class ValidateLicenseWhiteBoxTest {
	private Boolean expectedResult;
	private String licensePlate;
	
	
	public ValidateLicenseWhiteBoxTest( String licensePlate,Boolean expectedResult) {
		super();
		this.licensePlate=licensePlate;
		this.expectedResult = expectedResult;
	}
	

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{  "28482r",false },
			{ "A101234567",false },
			{ "99*2j90244",false },
			{ "99-s98de23",false },
			{ "24-A^24e23",false },
			{ "34-AA34mei",false },
			{ "34-AA29443",true },
		});
	}

	@Test
	public void testValidateLicensePlate() {

		assertEquals(
				"testValidateLicensePlate WhiteBox wrong on :",
				this.expectedResult, 
				BikeAddPane.validateLicensePlate(this.licensePlate)
		);
	}
}