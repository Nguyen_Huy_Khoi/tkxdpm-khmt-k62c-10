package com.ecobike.api;

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class RsClientHandleApi implements IHandleApi {
	
	private static IHandleApi singleton = new RsClientHandleApi();
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	private RsClientHandleApi() {
		client = ClientBuilder.newClient();
	}
	
	public static IHandleApi getInstance() {
		return singleton;
	}
	
	public ArrayList<Bike> getAllBike(String path) {
		WebTarget webTarget = client.target(PATH).path(path).path("all");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>(){});
		return res;
	}

	@Override
	public <E> E create(String path, E obj) {
		WebTarget webTarget = client.target(PATH).path(path).path("create");
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(obj, MediaType.APPLICATION_JSON));
		
		E res = (E) response.readEntity(obj.getClass());
		return res;	
	}

	@Override
	public ArrayList<Station> getAllStation() {
		WebTarget webTarget = client.target(PATH).path(ApiConstant.STATION_PATH).path("all");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>(){});
		return res;
	}
}
