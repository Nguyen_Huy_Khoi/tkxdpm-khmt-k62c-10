package com.ecobike.factory;

import java.util.HashMap;

import javax.swing.JFrame;

import com.ecobike.page.controller.AController;
import com.ecobike.page.gui.APage;

public class ControllerFactory {
	
	private static ControllerFactory instance;
	
	private HashMap m_registeredControllers=new HashMap();
	
	private ControllerFactory(){
		System.out.println("Singleton(): Initializing Instance ControllerFactory"); 
	}
		
	public static ControllerFactory getInstance(){
		if (instance == null){ 
			synchronized(ControllerFactory.class){
				if (instance == null){ 
					instance = new ControllerFactory();
				} 
			}
		};
		return instance;
	}
	
	
	public void registerController(String controllerId, AController p) {
		System.out.println("Register Controller():"+controllerId); 
		m_registeredControllers.put(controllerId, p);
	}
	
	public AController createController(String controllerId) {
		return   ((AController)m_registeredControllers.get(controllerId)).createController();
	}
}	
