	Thư mục chứa mã nguồn cuối cùng của nhóm .
	Cách cài đặt + chạy :
        + Mở 2 project Client và Server bằng IDE Eclipse 
	+ Chạy Server bằng cách chạy hàm main trong lớp com.ecobike.EcobikeServer
	+ Chạy tiếp Client bằng cách chạy hàm main trong lớp :com.ecobike.main.AdminMain
	+ Chạy module kiểm thử đơn vị bằng cách chạy lớp: com.ecobike.test.ValidateLicensePlateText	
	+ Lưu ý: chạy server trước client vì trong client có những màn tự động gửi request để nhận dữ liệu.