package com.ecobike.page.controller;

import javax.swing.JFrame;

import com.ecobike.page.gui.APage;

public abstract class AController {
	public abstract AController createController();
	private APage page;
	
	public AController(APage page) {
		this.page=page;
	}
	
	public AController() {
		
	}

	public APage getPage() {
		return page;
	}

	public void setPage(APage page) {
		this.page = page;
	}
	
	
}
