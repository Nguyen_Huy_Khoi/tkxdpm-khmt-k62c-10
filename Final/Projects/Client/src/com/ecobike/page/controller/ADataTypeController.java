package com.ecobike.page.controller;

import java.util.ArrayList;


public abstract class ADataTypeController<T> extends AController {
	
	public ADataTypeController() {

	}
	
	protected abstract ArrayList<T> getAll();


	public abstract T create(T t);
}
