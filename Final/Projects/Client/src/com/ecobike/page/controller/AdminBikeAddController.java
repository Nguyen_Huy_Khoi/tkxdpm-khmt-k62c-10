package com.ecobike.page.controller;

import java.util.ArrayList;

import javax.swing.JPanel;

import com.ecobike.api.ApiConstant;
import com.ecobike.api.MediaApi;
import com.ecobike.bean.Bike;
import com.ecobike.bean.Station;
import com.ecobike.components.bike.ebike.gui.EBikeAddPane;
import com.ecobike.components.bike.singlebike.gui.SingleBikeAddPane;
import com.ecobike.components.bike.twinbike.gui.TwinBikeAddPane;
import com.ecobike.factory.ControllerFactory;
import com.ecobike.factory.FactoryItemId;
public class AdminBikeAddController extends ADataTypeController<Bike>{
	static {
		ControllerFactory.getInstance().registerController(
				FactoryItemId.BIKE_ADD_PAGE
				, 
				new AdminBikeAddController()
		);
	}
	
	private ArrayList<Station> stations;
	
	public AdminBikeAddController() {
		stations=this.getAllStation();
		System.out.println("Save stations data on BikeAddController "+stations.size());
	}

	
	public ArrayList<Station> getStations() {
		return stations;
	}


	public void setStations(ArrayList<Station> stations) {
		this.stations = stations;
	}


	@Override
	public AController createController() {
		return new AdminBikeAddController();
	}
	
	public JPanel getSingleBikePane() {
		return new SingleBikeAddPane(this);
	}
	
	public JPanel getTwinBikePane() {
		return new TwinBikeAddPane(this);
	}
	
	public JPanel getEBikePane() {
		return new EBikeAddPane(this);
	}

	protected ArrayList<Station> getAllStation() {
		ArrayList<Station> res = (new MediaApi()).getAllStation();
		System.out.println(res);
		return (new MediaApi()).getAllStation();
	}

	@Override
	public Bike create(Bike t) {
		Bike resBike=(Bike) (new MediaApi()).create(t);
		setStations((new MediaApi().getAllStation()));
		return resBike;
	}

	@Override
	protected ArrayList<Bike> getAll() {
		return (new MediaApi()).getAllBike(ApiConstant.BIKE_PATH);
	}



}
