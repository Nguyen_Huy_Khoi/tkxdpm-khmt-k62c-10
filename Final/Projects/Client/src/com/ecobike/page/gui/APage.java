package com.ecobike.page.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.ecobike.page.controller.AController;

public abstract class APage extends JPanel {
	private AController controller;
	
	
	public AController getController() {
		return controller;
	}


	public void setController(AController controller) {
		this.controller = controller;
	}


	public abstract APage createPage(JFrame parent);
}	
