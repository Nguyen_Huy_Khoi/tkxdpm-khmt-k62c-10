package com.ecobike.page.gui;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ecobike.factory.FactoryItemId;
import com.ecobike.factory.PageFactory;
import com.ecobike.main.AdminMain;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminBikeManagePage extends APage {

	static {
		PageFactory.getInstance().registerPage(
				FactoryItemId.BIKE_MANAGE_PAGE
				, 
				new AdminBikeManagePage()
		);
	}
	
	private JPanel contentPane;


	@Override
	public APage createPage(JFrame parent) {
		return new AdminBikeManagePage(parent);
	}
	
	public AdminBikeManagePage() {
		
	}
	
	public AdminBikeManagePage(JFrame parent) {

		
		this.setLayout(null);
		
		JLabel header = new JLabel("Quản lý xe");
		header.setFont(new Font("Tahoma", Font.PLAIN, 25));
		header.setBounds(250, 15, 280, 30);
		this.add(header);
		
		JButton btnBikeList = new JButton("Xem danh sách xe");
		btnBikeList.setBounds(120, 80, 160, 80);
		this.add(btnBikeList);
	
			
		JButton btnBikeAddType = new JButton("Thêm loại xe mới");
		btnBikeAddType.setBounds(120, 200, 160, 80);
		this.add(btnBikeAddType);
		
		JButton btnBikeAdd = new JButton("Thêm xe");
		btnBikeAdd.setBounds(360, 80, 160, 80);
		this.add(btnBikeAdd);
		
		btnBikeAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Chuyển sang màn Thêm xe");
				((AdminMain)parent).displayPage(FactoryItemId.BIKE_ADD_PAGE);
			}
		});
		
		
		JButton btnBikeChangeCost = new JButton("Thay đổi giá thuê");
		btnBikeChangeCost.setBounds(360, 200, 160, 80);
		this.add(btnBikeChangeCost);
		
		
		JButton btnBack = new JButton("Quay lại");
		btnBack.setBounds(10, 10, 80, 30);
		this.add(btnBack);
	
		
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Quay lại màn Trang chủ");
				((AdminMain)parent).displayPage(FactoryItemId.ADMIN_HOME_PAGE);
			}
		});
		

	}


}
