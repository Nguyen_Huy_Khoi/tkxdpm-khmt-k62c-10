package com.ecobike.page.gui;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import com.ecobike.bean.Bike;
import com.ecobike.factory.FactoryItemId;
import com.ecobike.factory.PageFactory;
import com.ecobike.main.AdminMain;
import com.ecobike.page.controller.AController;
import com.ecobike.page.controller.AdminBikeAddController;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminBikeAddPage extends ADataTypePage<Bike> {

	private JTabbedPane tabbedPane;
	private AdminBikeAddController controller;
	static {
		PageFactory.getInstance().registerPage(
				FactoryItemId.BIKE_ADD_PAGE
				, 
				new AdminBikeAddPage()
		);
	}
	
	private JPanel contentPane;


	@Override
	public APage createPage(JFrame parent) {
		
		return new AdminBikeAddPage(parent);
	}
	
	public AdminBikeAddPage() {
		
	}
	
	public AdminBikeAddPage(JFrame parent) {
		
		super(parent);
		this.setLayout(null);
		
		JLabel header = new JLabel("Thêm xe");
		header.setFont(new Font("Tahoma", Font.PLAIN, 25));
		header.setBounds(250, 15, 280, 30);
		this.add(header);
		
		JButton btnBack = new JButton("Quay lại");
		btnBack.setBounds(10, 10, 80, 30);
		this.add(btnBack);
	
		
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Quay lại màn Trang chủ");
				((AdminMain)parent).displayPage(FactoryItemId.BIKE_MANAGE_PAGE);
			}
		});

		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(50, 50, 520, 320);
		this.add(tabbedPane);
		
		
	}
	
	public void setController(AController controller) {
		this.controller=(AdminBikeAddController) controller;
		
		JPanel singleBikePage =  ((AdminBikeAddController) controller).getSingleBikePane();
		JPanel twinBikePage = ((AdminBikeAddController) controller).getTwinBikePane();
		JPanel eBikePage = ((AdminBikeAddController) controller).getEBikePane();
		
		
		tabbedPane.addTab("Xe đạp đơn", null, singleBikePage, "Thêm xe đạp đơn");
		tabbedPane.addTab("Xe đạp đôi", null, twinBikePage, "Thêm xe đạp đôi");
		tabbedPane.addTab("Xe đạp điện", null, eBikePage, "Thêm xe đạp điện");

		BorderLayout layout = new BorderLayout();

	}


}
