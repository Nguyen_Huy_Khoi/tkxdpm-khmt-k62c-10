package com.ecobike.factory;

import java.util.HashMap;

import javax.swing.JFrame;

import com.ecobike.page.gui.APage;

public class PageFactory {
	
	private static PageFactory instance;
	
	private HashMap m_registeredPages=new HashMap();
	
	private PageFactory(){
		System.out.println("Singleton(): Initializing Instance PageFactory"); 
	}
		
	public static PageFactory getInstance(){
		if (instance == null){ 
			synchronized(PageFactory.class){
				if (instance == null){ 
					instance = new PageFactory();
				} 
			}
		};
		return instance;
	}
	
	
	public void registerPage(String pageId, APage p) {
		System.out.println("Register Page():"+pageId); 
		m_registeredPages.put(pageId, p);
	}
	
	public APage createPage(String pageId,JFrame parent) {
		return   ((APage)m_registeredPages.get(pageId)).createPage(parent);
	}
}	
