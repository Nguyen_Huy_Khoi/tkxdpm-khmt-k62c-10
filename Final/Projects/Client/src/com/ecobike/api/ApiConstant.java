package com.ecobike.api;

public class ApiConstant {

	public static String BIKE_PATH="bike";
	public static String SINGLEBIKE_PATH="singlebike";
	public static String EBIKE_PATH="ebike";
	public static String TWINBIKE_PATH="twinbike";
	public static String STATION_PATH="station";


}
