package com.ecobike.api;

import java.util.ArrayList;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;

public interface IHandleApi {
	public abstract  ArrayList<Bike> getAllBike(String path);
	public abstract <E> E create(String path,E obj);
	
	public abstract  ArrayList<Station> getAllStation(	);
}
