package com.ecobike.api;

import java.util.ArrayList;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;

public class MediaApi {
	
	
	public MediaApi() {
	}
	
	
	public ArrayList<Bike> getAllBike(String path) {
		return RsClientHandleApi.getInstance().getAllBike(path);
	}
	
	public Object create(Object obj) {
		System.out.println("Media API "+obj.toString());
		System.out.println("Media API "+obj.getClass());
		System.out.println("Media API "+obj.getClass().getSimpleName().toLowerCase());
		
		
		return RsClientHandleApi.getInstance().create(
				obj.getClass().getSimpleName().toLowerCase(),  //path of beans 
				obj  // instance of beans
			);
	}
	
	public ArrayList<Station> getAllStation(){
		return  RsClientHandleApi.getInstance().getAllStation();
	}
}
