package com.ecobike.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;


@JsonTypeName("station")

public class Station {
	private String id;
	private String stationName;
	private String stationAddress;
	private int numberOfSingleBikes;
	private int numberOfEBikes;
	private int numberOfTwinBikes;
	private int numberOfEmptyDocks;

	public Station() {
		super();
	}
	

	
	public Station(String id, String stationName, String stationAddress, int numberOfSingleBikes, int numberOfEBikes,
			int numberOfTwinBikes, int numberOfEmptyDocks) {
		super();
		this.id = id;
		this.stationName = stationName;
		this.stationAddress = stationAddress;
		this.numberOfSingleBikes = numberOfSingleBikes;
		this.numberOfEBikes = numberOfEBikes;
		this.numberOfTwinBikes = numberOfTwinBikes;
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getStationName() {
		return stationName;
	}



	public void setStationName(String stationName) {
		this.stationName = stationName;
	}



	public String getStationAddress() {
		return stationAddress;
	}



	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}



	public int getNumberOfSingleBikes() {
		return numberOfSingleBikes;
	}



	public void setNumberOfSingleBikes(int numberOfSingleBikes) {
		this.numberOfSingleBikes = numberOfSingleBikes;
	}



	public int getNumberOfEBikes() {
		return numberOfEBikes;
	}



	public void setNumberOfEBikes(int numberOfEBikes) {
		this.numberOfEBikes = numberOfEBikes;
	}



	public int getNumberOfTwinBikes() {
		return numberOfTwinBikes;
	}



	public void setNumberOfTwinBikes(int numberOfTwinBikes) {
		this.numberOfTwinBikes = numberOfTwinBikes;
	}



	public int getNumberOfEmptyDocks() {
		return numberOfEmptyDocks;
	}



	public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}



	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			return this.id.equals(((Station) obj).id);
		}
		return false;
	}



	@Override
	public String toString() {
		return  stationName +"(Còn "+numberOfEmptyDocks +" chỗ trống)";
	}

	
	
	
	
	


}