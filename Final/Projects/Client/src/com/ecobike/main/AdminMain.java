package com.ecobike.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ecobike.factory.ControllerFactory;
import com.ecobike.factory.FactoryItemId;
import com.ecobike.factory.PageFactory;
import com.ecobike.page.controller.AController;
import com.ecobike.page.gui.APage;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JTextField;

public class AdminMain extends JFrame {
	public JPanel rootPanel;
	static { 
		try{ 
			Class.forName("com.ecobike.page.gui.AdminHomePage"); 
			Class.forName("com.ecobike.page.gui.AdminBikeManagePage");
			Class.forName("com.ecobike.page.gui.AdminBikeAddPage"); 
			Class.forName("com.ecobike.page.controller.AdminHomeController");
			Class.forName("com.ecobike.page.controller.AdminBikeManageController"); 
			Class.forName("com.ecobike.page.controller.AdminBikeAddController");
		}
		catch (ClassNotFoundException any){
			any.printStackTrace(); } 
	}
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminMain frame = new AdminMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void displayPage(String id) {
		APage page=PageFactory.getInstance().createPage(id, this);
		AController controller=ControllerFactory.getInstance().createController(id);
		
		page.setController(controller);
		controller.setPage(page);
		
		this.rootPanel.removeAll();
		this.rootPanel.add(page);
		this.rootPanel.updateUI();
	
	};

	
	
	public AdminMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 640, 420);
		
		rootPanel=new JPanel();
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		setContentPane(rootPanel);
			
		this.displayPage(FactoryItemId.ADMIN_HOME_PAGE);

	}
}
