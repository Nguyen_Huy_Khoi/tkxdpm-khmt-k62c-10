package com.ecobike.test;
import org.junit.Test;
import org.junit.platform.engine.support.filter.ClasspathScanningSupport;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ecobike.components.bike.gui.BikeAddPane;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

@RunWith(Parameterized.class)
public class ValidateLicensePlateBlackBoxTest {
	private String licensePlate;
	private Boolean expectedResult;
	
	
	public ValidateLicensePlateBlackBoxTest( String licensePlate,Boolean expectedResult) {
		super();
		this.licensePlate=licensePlate;
		this.expectedResult = expectedResult;
	}
	

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{  "ASD12",false },
			{ "2d12345678",false },
			{ "5731we3r4t",false },
			{ "34-e3234S3",false },
			{ "23-AD32fe3",false },
			{ "26-AA94834",true }
		});
	}
	
	@Test
	public void testValidateLicensePlate() {

		
		assertEquals(
				"testValidateLicensePlate BlackBox wrong on :",
				this.expectedResult, 
				BikeAddPane.validateLicensePlate(this.licensePlate)
		);
	}
}