package com.ecobike.components.bike.gui;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecobike.bean.Bike;
import com.ecobike.bean.Station;
import com.ecobike.components.abstracts.gui.ADataAddPane;
import com.ecobike.page.controller.ADataTypeController;
import com.ecobike.page.controller.AdminBikeAddController;


@SuppressWarnings("serial")
public class BikeAddPane extends ADataAddPane<Bike> {
		
	
	private JTextField nameField;
	private JTextField typeField;
	private JFormattedTextField weightField;
	private JTextField licensePlateField;
	private JTextField manufacturingDateField;
	private JTextField producerField;
	private JFormattedTextField costField;
	JComboBox stationPicker;

	public BikeAddPane(ADataTypeController<Bike> controller) {
		super(controller);
		this.t=new Bike();
		getTypeField().setText("Xe đạp");
		getTypeField().disable();
	}

	
	@Override
	public void buildControls() {
		JLabel titleLabel = new JLabel("Tên xe:");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(titleLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		
		JLabel typeLabel = new JLabel("Loại xe:");
		setTypeField(new JTextField(15));
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(typeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(getTypeField(), c);
		
		typeField.disable();
		
		
		JLabel weightLabel = new JLabel("Trọng lượng(kg):");
		weightField = new JFormattedTextField(numberFormat);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(weightLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(weightField, c);
		
		JLabel licensePlateLabel = new JLabel("Biển số:");
		licensePlateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(licensePlateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(licensePlateField, c);
		
		JLabel manuafacturingDateLabel = new JLabel("Ngày sản xuất:");
		manufacturingDateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(manuafacturingDateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(manufacturingDateField, c);
		
		JLabel producerLabel = new JLabel("Nhà sản xuất:");
		producerField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(producerLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(producerField, c);
		
		
		JLabel costLabel = new JLabel("Giá tiền:");
		costField =new JFormattedTextField(numberFormat);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(costLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(costField, c);
		
		JLabel stationLabel = new JLabel("Bãi xe:");
		ArrayList<Station> stations=((AdminBikeAddController)this.controller).getStations();
		stationPicker=new JComboBox(stations.toArray());    
	    
		System.out.println("Stations Number :"+stations.size());
	    row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(stationLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(stationPicker, c);
		
    
	}

	@Override
	public void resetFields() {
		this.nameField.setText("");
		this.weightField.setText("");
		this.licensePlateField.setText("");
		this.manufacturingDateField.setText("");
		this.producerField.setText("");
		this.costField.setText("");
		
		stationPicker.removeAllItems();
		
		System.out.println("Reset Fields :");
		ArrayList<Station> stations=((AdminBikeAddController)this.controller).getStations();
	    
		for (Station station : stations) {
			stationPicker.addItem(station);
			System.out.println("Reset Fields Station_EmptyDocks :"+station.getNumberOfEmptyDocks());
		};
	}
	
	@Override
	public Bike getObject() {
		t.setName(nameField.getText());
		t.setType(getTypeField().getText());
		t.setWeight(Float.valueOf( weightField.getText().replace(",", "")));
		t.setLicensePlate(licensePlateField.getText());
		t.setManufacturingDate(manufacturingDateField.getText());
		t.setProducer(producerField.getText());
		
		System.out.println("costField:"+costField.getText());
		t.setCost(Float.valueOf(costField.getText().replace(",", "")));
		
		t.setStationId(((Station)stationPicker.getSelectedItem()).getId());
		System.out.println("StationID:"+t.getStationId());
		return t;
	}

	
	//Biển số xe có định dạng chuẩn như sau : 
	// XX-YYZZZZZ (10 ký tự) trong đó :
	// XX : 00 -> 99
	// YY : 2 chữ cái in hoa
	// ZZZZZ : 00000 -> 99999
	
	public static boolean isNumeric(String str) { 
		System.out.println("Test   "+str);
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}
	
	
	public static boolean validateLicensePlate(String str) { 
		if (str.length()!=10) return false;
		if (!isNumeric(str.substring(0, 2))) return false;
		if (str.charAt(2)!='-') return false;
		if (!Character.isUpperCase(str.charAt(3))) return  false;
		if (!Character.isUpperCase(str.charAt(4))) return  false;
		if (!isNumeric(str.substring(5, 10))) return false;
		return true;
		
		
	}
	@Override
	public boolean validateFields() {
		if ("".equals(nameField.getText())) return false;
		if ("".equals(getTypeField().getText())) return false;
		if ("".equals(weightField.getText())) return false;
		if ("".equals(licensePlateField.getText())) return false;
		if ("".equals(manufacturingDateField.getText())) return false;
		if ("".equals(producerField.getText())) return false;
		if ("".equals(costField.getText())) return false;
		if (stationPicker.getSelectedItem()==null)return false ;
		
		
		if (!this.validateLicensePlate(licensePlateField.getText())) return false;
		return true;	
	}

	public JTextField getTypeField() {
		return typeField;
	}

	public void setTypeField(JTextField typeField) {
		this.typeField = typeField;
	}
}
