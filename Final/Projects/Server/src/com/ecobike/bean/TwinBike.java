package com.ecobike.bean;

public class TwinBike extends Bike {

	public TwinBike() {
		super();
	}
	
	public TwinBike(String id, String name, String type, float weight, String licensePlate,	
			String manufacturingDate, String producer, float cost,String stationId) {
		super(id,name,type,weight,licensePlate,manufacturingDate,producer,cost,stationId);
	}
}