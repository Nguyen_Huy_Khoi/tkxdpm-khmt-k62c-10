package com.ecobike.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.TwinBike;
import com.ecobike.db.IDatabase;
import com.ecobike.db.JsonDatabase;

@Path("/singlebike")
public class SingleBikeService {
	
	private IDatabase database;
	
	public SingleBikeService() {
		database = JsonDatabase.singleton();
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getAllSingleBike() {
		ArrayList<Bike> res = database.getAllBike(SingleBike.class);
	    return res;
	}
	    
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SingleBike createSingleBike(SingleBike bike) {
		System.out.println("SingleBikeService CreateBike :"+bike.toString());
		return database.createBike(bike);
	}
}