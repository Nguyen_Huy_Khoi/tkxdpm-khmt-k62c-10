package com.ecobike.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.TwinBike;
import com.ecobike.db.IDatabase;
import com.ecobike.db.JsonDatabase;

@Path("/twinbike")
public class TwinBikeService {
	
	private IDatabase database;
	
	public TwinBikeService() {
		database = JsonDatabase.singleton();
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getAllTwinBike() {
		ArrayList<Bike> res = database.getAllBike(TwinBike.class);
	    return res;
	}
	    
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike createTwinBike(TwinBike bike) {
		System.out.println("TwinBikeService CreateBike :"+bike.toString());
		return database.createBike(bike);
	}
}