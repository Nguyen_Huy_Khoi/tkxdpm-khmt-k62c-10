package com.ecobike;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.ecobike.service.BikeService;
import com.ecobike.service.EBikeService;
import com.ecobike.service.SingleBikeService;
import com.ecobike.service.StationService;
import com.ecobike.service.TwinBikeService;

public class EcobikeServer {
	public static final int PORT = 8080;
	
	public static void main(String[] args) throws Exception {
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(PORT);
		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);

		
		jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
				StationService.class.getCanonicalName() + ", " +
				BikeService.class.getCanonicalName() + ", " + 
				SingleBikeService.class.getCanonicalName() + ", " + 
				EBikeService.class.getCanonicalName() + ", " +
				TwinBikeService.class.getCanonicalName()
		);

		
		try {
			jettyServer.start();
			jettyServer.join();
		} finally {
			jettyServer.destroy();
		}
	}
}