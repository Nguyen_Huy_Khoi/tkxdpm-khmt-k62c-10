package com.ecobike.db;

import java.util.ArrayList;
import java.sql.*;
import com.ecobike.bean.Bike;
import com.ecobike.bean.EBike;
import com.ecobike.bean.SingleBike;
import com.ecobike.bean.Station;
import com.ecobike.bean.TwinBike;
import com.ecobike.db.seed.Seed;

public class JsonDatabase implements IDatabase{
	private static IDatabase singleton = new JsonDatabase();
	
	private ArrayList<Bike> bikes = Seed.singleton().getBikes();
	private ArrayList<Station> stations = Seed.singleton().getStations();
	
	private JsonDatabase() {
	}
	
	public static IDatabase singleton() {
		return singleton;
	}

	@Override
	public  ArrayList<Bike> getAllBike(Class<?> bikeClass) {
		
		System.out.println("BikeClass :"+bikeClass);
		
		ArrayList<Bike> res=new ArrayList<>();
		for (Bike bike : bikes) {
		//	System.out.println("Class in arr :"+bike.getClass());
			if (bikeClass == bike.getClass()
			|| bikeClass == bike.getClass().getSuperclass()) {
				res.add(bike);
			}
		}
		return res;
	}
	
	@Override
	public <E> E createBike(E e) {
		Bike bike=(Bike)e;
		for (Bike b: bikes) {
			if (b.equals(bike)) {
				return null;
			}
		};
		
		System.out.println("Station_ID bike  :"+bike.getStationId());
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		( bike).setId(""+System.currentTimeMillis());
		bikes.add( bike);
		
		for (Station station:stations) {
		
			if (station.getId().equals(((Bike)bike).getStationId())) {
				
				System.out.println("Pre EmptyDocks   :"+station.getNumberOfEmptyDocks());
				station.setNumberOfEmptyDocks(
						station.getNumberOfEmptyDocks()-1
						);
				System.out.println("After EmptyDocks   :"+station.getNumberOfEmptyDocks());
				System.out.println("Station_ID  :"+station.getId());
			}
		}
		return e;
	}
	
	@Override
	public  ArrayList<Station> getAllStation() {
		ArrayList<Station> availStations=new ArrayList<>();
		for (Station station:stations) {
			if (station.getNumberOfEmptyDocks()>0) {
				availStations.add(station);
			}
		}
		return availStations;
	}
	
	
	
	

}
