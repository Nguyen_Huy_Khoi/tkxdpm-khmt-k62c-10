package com.ecobike.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.ecobike.bean.Bike;
import com.ecobike.bean.Station;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Seed {
	private ArrayList<Bike> bikes;
	private ArrayList<Station> stations;
	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		bikes = new ArrayList<Bike>();
		bikes.addAll(readBikesFromFile( new File(getClass().getResource("./singlebikes.json").getPath()).toString()));
		bikes.addAll(readBikesFromFile( new File(getClass().getResource("./ebikes.json").getPath()).toString()));
		bikes.addAll(readBikesFromFile( new File(getClass().getResource("./twinbikes.json").getPath()).toString()));
		
		stations=new ArrayList<Station>();
		stations.addAll(readStationsFromFile( new File(getClass().getResource("./stations.json").getPath()).toString()));
	}
	
	private ArrayList<? extends Bike> readBikesFromFile(String filePath){
		ArrayList<? extends Bike> res = new ArrayList<Bike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<Station> readStationsFromFile(String filePath){
		ArrayList<Station> res = new ArrayList<Station>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}

	public ArrayList<Bike> getBikes() {
		return bikes;
	}
	
	public ArrayList<Station> getStations() {
		return stations;
	}
	
	
	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
}
